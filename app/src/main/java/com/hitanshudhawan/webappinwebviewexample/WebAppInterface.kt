package com.hitanshudhawan.webappinwebviewexample

import android.content.Context
import android.webkit.JavascriptInterface
import android.widget.Toast

class WebAppInterface(val context: Context) {

    @JavascriptInterface
    fun showToast(toastMessage: String) {
        Toast.makeText(context, toastMessage,Toast.LENGTH_SHORT).show()
    }
}