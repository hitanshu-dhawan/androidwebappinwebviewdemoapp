package com.hitanshudhawan.webappinwebviewexample

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        web_view.settings.javaScriptEnabled = true
        web_view.addJavascriptInterface(WebAppInterface(this),"Hit")
        web_view.webViewClient = MyWebViewClient()
        web_view.loadUrl("file:///android_asset/webapp.html")
    }

    override fun onBackPressed() {
        if (web_view.canGoBack()) web_view.goBack()
        else super.onBackPressed()
    }
}
